<?php

$GLOBALS['config'] = [
    'mysql' => [
        'server' => 'localhost',
        'user' => 'root',
        'pass' => '',
        'dbname' => 'gb'
    ]
];

spl_autoload_register(function($class) {
    require_once $class . '.php';
});