<?php

/**
 * Class DB
 *
 * The database class. Used PDO for database calls. Accessible only by static instantiation using getInstance()
 * Example $db = DB::getInstance();
 */

class DB
{
    private static $_instance = null;
    private $_pdo,
        $_query,
        $_error = false,
        $_results,
        $_count = 0;

    /**
     * __construct (private: accessible only by this class) - Class constructor. Created the PDO object for database use
     */
    private function __construct()
    {
        try
        {
            $this->_pdo = new PDO('mysql:host=' . Config::get('mysql/server') . ';dbname='. Config::get('mysql/dbname'), Config::get('mysql/user'), Config::get('mysql/pass'));
        }
        catch(PDOException $ex)
        {
            die($ex->getMessage());
        }
    }

    /**
     * Static Method getInstance (public: accessible everywhere) - A recursive method, getInstance() instantiates the class
     * without having to manually reinstantiate it and causing you to constantly reconnect to the database
     *
     * @return DB|null - Returns a PDO object if connection was successfull, null if not
     */
    public static function getInstance()
    {
        if(!isset(self::$_instance))
        {
            self::$_instance = new DB();
        }
        return self::$_instance;
    }

    /**
     * Method query (public: accessible after getting instance) - Performs a PDO execution through a MySQL query
     *
     * @param $sql - The query being parsed
     * @param array $params - Array of parameters (The values passing through)
     * @return $this - Returns itself
     */
    public function query($sql, $params = array())
    {
        $this->_error = false;
        if($this->_query = $this->_pdo->prepare($sql))
        {
            if(count($params))
            {
                $x = 1;
                foreach($params as $param)
                {
                    $this->_query->bindValue($x, $param);
                    $x++;
                }
            }

            if($this->_query->execute())
            {
                $this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
                $this->_count = $this->_query->rowCount();
            }
            else
            {
                $this->_error = true;
            }
        }

        return $this;
    }

    /**
     * Method action (public: accessible after getting instance) - Performs an SQL action using the query method
     *
     * @param $action - The action to be performed (an SQL query)
     * @param $table - The table the query is performing against
     * @param array $where - Where in the table the action needs to be performed (the row)
     * @return $this|bool
     */
    public function action($action, $table, $where = array())
    {
        if(count($where) === 3)
        {
            $operators = array('=', '>', '<', '>=', '<=');

            $field 		= $where[0];
            $operator 	= $where[1];
            $value 		= $where[2];

            if(in_array($operator, $operators))
            {
                $sql = "{$action} FROM {$table} WHERE {$field} {$operator} ?";
                if(!$this->query($sql, array($value))->error())
                {
                    return $this;
                }
            }
        }

        return false;
    }

    /**
     * Method get (public: accessible after getting instance) - Gets data from the database
     *
     * @param $table - The table the action should be performed on
     * @param array $where - Where the data should be grabbed from ex: DB::getInstance()->get('mytable', array('username', '=', 'myusername'));
     * @return $this|bool
     */
    public function get($table, $where = array())
    {
        return $this->action("SELECT *", $table, $where);
    }

    /**
     * Method delete (public: accessible after getting instance) - Deletes data from the database
     *
     * @param $table - The table the action should be performed on
     * @param $where - Where the data should be deleted from
     * @return $this|bool
     */
    public function delete($table, $where)
    {
        return $this->action("DELETE ", $table, $where);
    }

    /**
     * Method insert (public: accessible after getting instance) - Inserts data into the database
     *
     * @param $table - The table the action should be performed on
     * @param array $fields - The values that need to be inserted into the database
     * @return bool - Returns true if query was successful, false if not
     */
    public function insert($table, $fields = array())
    {
        if(count($fields))
        {
            $keys = array_keys($fields);
            $values = '';
            $x = 1;

            foreach($fields as $field)
            {
                $values .= "?";
                if($x < count($fields))
                {
                    $values .= ', ';
                }
                $x++;
            }

            $sql = "INSERT INTO {$table} (`" . implode('`, `', $keys) . "`) VALUES ({$values})";

            if(!$this->query($sql, $fields)->error())
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Method update (public: accessible after getting instance) - Updates certain data within the database
     *
     * @param $table - The table the action should be performed on
     * @param $id - The identifier for the row being updated
     * @param $fields - The values being updated
     * @return bool - Returns true if the query was successful, false if not
     */
    public function update($table, $id, $fields)
    {
        $set = '';
        $x = 1;

        foreach($fields as $name => $value)
        {
            $set .= "{$name} = ?";
            if($x < count($fields))
            {
                $set .= ', ';
            }
            $x++;
        }

        $sql = "UPDATE {$table} SET {$set} WHERE id = {$id}";

        if(!$this->query($sql, $fields)->error())
        {
            return true;
        }
        return false;
    }

    /**
     * Method count (public: accessible after getting instance) - Gets a query's count.
     *
     * @return int - Returns 0 if failed query, or the number of successful passes within the query
     */
    public function count()
    {
        return $this->_count;
    }

    /**
     * Method results (public: accessible after getting instance) - Returns the results after using the get() method
     *
     * @param string $index - The index is used if you are looking to iterate through every result.
     * @return mixed - Returns a single indexed result if an index is provided, or the entire query's result if not
     */
    public function results($index = '')
    {
        if(is_numeric($index))
        {
            return $this->single_result($index);
        }
        else
        {
            return $this->_results;
        }
    }

    /**
     * Method single_result (private: accessible only by this class) - If an index is passed within the results() method
     * This method is used to single out that result and is returned within the results() method
     *
     * @param $index - The index supplied by the results() method
     * @return mixed - returns the single result for use with the results() method
     */
    private function single_result($index)
    {
        return $this->_results[$index];
    }

    /**
     * Method first (public: accessible after getting instance) - Returns only the first result out of all the results
     *
     * @return mixed - Returns only the first result in the results list
     */
    public function first()
    {
        return $this->_results[0];
    }

    /**
     * Method error (public: accessible after getting instance) - Returned if an error has occurred within the class
     *
     * @return bool - Returns true if an error was thrown, false if not
     */
    public function error()
    {
        return $this->_error;
    }
}
