<?php

if(Input::exists())
{
    $name = Input::get('name');
    $email = Input::get('email');
    $content = Input::get('message');

    if(!empty($name))
    {
        if(!empty($email))
        {
            if(!empty($content))
            {
                $db->insert('posts', [ $name, $email, $content, time() ]);
                header("Location: ./");
            }
            else
            {
                echo "Please supply a message. Try again!";
            }
        }
        else
        {
            echo "Please supply your email. Try again!";
        }
    }
    else
    {
        echo "Please supply your name. Try again!";
    }
}
else
{
    header("Location: ./");
}

?>

<form method="post" action="?page=post">
    <table>
        <tr>
            <td><label for="name">Name:</label></td>
            <td><input type="text" name="name" id="name"></td>
        </tr>
        <tr>
            <td><label for="email">Email:</label></td>
            <td><input type="email" name="email" id="email"></td>
        </tr>
        <tr>
            <td><label for="message">Message:</label></td>
            <td><textarea name="message" id="message" cols="17" rows="5"></textarea></td>
        </tr>
        <tr>
            <td><input type="submit" name="post" id="post" value="Post"></td>
            <td><input type="reset" value="Reset Form"></td>
        </tr>
    </table>
</form>

<a href="./">Home</a>