<?php

require_once 'core.php';

$db = DB::getInstance();

if(!isset($_GET['page']))
{
    require_once 'home.php';
}
else
{
    require_once $_GET['page'] . '.php';
}